"""Parser module to parse command line args from command.json"""
import os
import sys
import argparse
import datetime
from .util import get_nifti_file 
from .xnat_logging import stdout_log

parser = argparse.ArgumentParser()
parser.add_argument('--input_dir', required=True)
parser.add_argument('--output_dir', required=True)

parser.add_argument('-o', required=True,default="fsl_anat")
parser.add_argument('-t', required=True,default="T1")
parser.add_argument('-s', required=True,type=int,default=10)
parser.add_argument('--betfparam', required=True,default=0.1,type=float)

parser.add_argument('--weakbias', required=False, action='store_true')
parser.add_argument('--noreorient', required=False, action='store_true')
parser.add_argument('--nocrop', required=False, action='store_true')
parser.add_argument('--nobias', required=False, action='store_true')
parser.add_argument('--noreg', required=False, action='store_true')
parser.add_argument('--nononlinreg', required=False, action='store_true')
parser.add_argument('--noseg', required=False, action='store_true')
parser.add_argument('--nosubcortseg', required=False, action='store_true')
parser.add_argument('--nosearch', required=False, action='store_true')
parser.add_argument('--nocleanup', required=False, action='store_true')

args=parser.parse_args() 

def parse_config():
    """ Parses inputs and configurations from the command line and formats them.

    Args:
        args: An object containing parsed command-line arguments.

    Returns:
        tuple: A tuple containing:
            - list: A list of command-line arguments formatted for the `fsl_anat` command.
            - str: The formatted output folder path.
    """
    # get nifti file path
    nifti_file = get_nifti_file()
    
    # build up fsl_anat command
    cmd_list=["fsl_anat"]
    cmd_list.extend(["-i",nifti_file])

    for arg in vars(args):
        argument_name = arg
        argument_value = getattr(args, arg)
        
        if isinstance(argument_value, bool):         
            if argument_value:
                cmd_arg=f"--{argument_name}"
                cmd_list.append(cmd_arg)
        else:
            if argument_name == "s":
                cmd_arg=f"-{argument_name}"
                cmd_list.extend([cmd_arg,str(argument_value)])

            elif argument_name == "t":
                if argument_value not in ["T1","T2","PD"]:
                    stdout_log.error(f"{argument_value} is an invalid Image type. T1 T2 PD are possible options.")
                    sys.exit(1)
                else:
                    cmd_arg=f"-{argument_name}"
                    cmd_list.extend([cmd_arg,str(argument_value)])
            elif argument_name == "o":
                cmd_arg=f"-{argument_name}"
                timestamp = datetime.datetime.now()
                timestamp_str = timestamp.strftime("%m-%d-%Y_%H:%M:%S")

                output_folder=f"{os.path.join(args.output_dir,argument_value)}_{timestamp_str}"
                cmd_list.extend([cmd_arg,output_folder])
            elif argument_name == "betfparam":
                if argument_value > 1.0 or argument_value < 0:
                    stdout_log.error("betfparam can only be between 0 and 1")
                    sys.exit(1)
                else:
                    cmd_arg=f"--{argument_name}={str(argument_value)}"
                    cmd_list.append(cmd_arg)
    # skip registration if image type is T2 or PD
    if "PD" in cmd_list or "T2" in cmd_list:
        if "--noreg" not in cmd_list:
            cmd_list.append("--noreg")
        if "--nononlinreg" not in cmd_list:
            cmd_list.append("--nononlinreg")

        stdout_log.info(f"Registration will be skipped for {args.t}")

    return cmd_list,output_folder
