import sys
import subprocess
from .xnat_logging import stdout_log,stderr_log

def run(cmd_list):
    """Run fsl_anat

    Args:
        cmd_list (list): list of inputs + options for fsl_anat
    """
    stdout_log.info(f"Running {' '.join(cmd_list)}")
    process = subprocess.Popen(
                cmd_list,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                universal_newlines=True
            )
        
    # Stream and log the stdout
    for line in process.stdout:
        stdout_log.info(line.strip())

    process.wait()
    return_code = process.returncode
    
    if return_code != 0:
        stdout_log.error(f"fsl_anat returned a non-zero return code of {return_code}. Exiting! Check standard error log for more details.")
        for line in process.stderr:
            stderr_log.error(line.strip())
        sys.exit(1)
        
    elif return_code == 0:    
        return 0

