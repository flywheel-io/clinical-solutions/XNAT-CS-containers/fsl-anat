import os
import sys
from .xnat_logging import stdout_log

def get_nifti_file(nifti_dir="/input/NIFTI"):
    """Get input nifti image path

    Args:
        input_dir (str, optional): path to search under. Defaults to "/input/NIFTI".
    Returns:
        str: string path to NIFTI file 
    """
    if not os.path.exists(nifti_dir):
        stdout_log.error("This scan does not have a NIFTI resource!")
        sys.exit(1)
    else:   
        nifti_resource_files= os.listdir(nifti_dir)
        nii_files = [file for file in nifti_resource_files if ".nii.gz" in file or ".nii" in file]

        # Assuming that there is only 1 file under NIFTI resource. 
        #  This should be the case for T1, T2, PD datasets
        if len(nii_files) > 0:
            nifti_file = os.path.join(nifti_dir,nii_files[0])
            return nifti_file
        elif len(nii_files) == 0:
            stdout_log.error("No files under NIFTI resource!")
            sys.exit(1)


