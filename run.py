import os
import sys
import traceback

from fsl_anat.parser import parse_config
from fsl_anat.main import run
from fsl_anat.xnat_logging import stdout_log, stderr_log

def main():
    cmd_list,output_folder = parse_config()
    
    return_code = run(cmd_list)

    if return_code == 0:
        stdout_log.info("fsl_anat processing completed successfully.")
        os.rename(f"{output_folder}.anat",output_folder)


if __name__ == "__main__":
    try:
        main()
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1)
