# fsl-anat

This scan-level XNAT container wraps v6.0.7.7 FSL's [fsl_anat](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat), which is a general pipeline for processing anatomical images.

- [fsl-anat](#fsl-anat)
  - [Summary](#summary)
    - [Inputs](#inputs)
    - [Config](#config)
    - [Outputs](#outputs)
    - [Citation](#citation)

## Summary
- The input must be a 3D NIFTI file (.nii or .nii.gz) that is saved as a scan resource under the NIFTI folder. The container will specifically look for a folder called NIFTI under the scan, and assumes that there is only 1 NIFTI file (`fsl-anat` will always take the first .nii or .nii.gz file found as the input file).
- If the [ImageType](#config) is T2 or PD, registration will be automatically skipped (*--noreg* and *--nononlinreg* are enabled in this case).
- Sub-cortical segmentation (via FIRST) cannot be performed on a non-T1 image (when *ImageType* is T2 or PD), so be sure to enable *--nosubcortseg* prior to running on non-T1 images.
- If `fsl-anat` is re-run on scan, pre-existing outputs are not overwritten. Instead any outputs are saved under their own [timestamped folder](#outputs).
- The -d and --clobber options from [fsl_anat](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat) were not implemented in this container since those options are handled automatically during Container Service 'Finalization' step.
- LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.

- Performance:
    - `reserve-memory` in the command.json was set to 1000 (1GB)
    - Per [fsl_anat](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat), run-time is dependent on the resolution of the image and anywhere between 30-90 min would be typical.

### Inputs
- *Anatomical image*
  - __Type__: *NIFTI*
  - __Description__: *Anatomical T1/T2/PD NIFTI image saved under NIFTI scan resource*

### Config
- *Betfparam*
  - __Type__: *Number*
  - __Default__: *0.1*
  - __Description__: *f parameter (fractional intensity) for BET (this is only used if you're not running non-linear reg, and if you want brain extraction done). --betfparam in fsl_anat.*

- *ImageType*
  - __Type__: *String*
  - __Default__: *T1*
  - __Description__: *Specify the type of image. T1, T2, PD are possible options. -t in fsl_anat.*

- *BiasField_smoothing*
  - __Type__: *Number*
  - __Default__: *10*
  - __Description__: *Specify the value for bias field smoothing (-l option in FAST). -s in fsl_anat*
  
- *Weakbias*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Used for images with little and/or smooth bias fields. Also will overwrite BiasField_smoothing to be 20 when run. The bias-field correction assumes that the field is 'strong', typical of that arising from a multi-coil array or a high-field scanner. For images acquired using birdcage coils or on 1.5T scanners, the --weakbias option will be faster and may produce equally good results. --weakbias in fsl_anat.*
- *Noreorient*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off the step that does reorientation to standard (fslreorient2std). --noreorient in fsl_anat.*
- *Nocrop*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off step that does automated cropping (robustfov). --nocrop in fsl_anat.*
- *Nobias*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off steps that do bias field correction (via FAST). --nobias in fsl_anat.*
- *Noreg*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off steps that do registration to standard (FLIRT and FNIRT). --noreg in fsl_anat.*
- *Nononlinreg*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off step that does non-linear registration (FNIRT). --nononlinreg in fsl_anat.*
- *Noseg*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off step that does tissue-type segmentation (FAST). --noseg in fsl_anat.*
- *Nosubcortseg*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Turn off step that does sub-cortical segmentation (FIRST). --nosubcortseg in fsl_anat.*
  - __Note__: *Sub-cortical segmentation (via FIRST) cannot be performed on a non-T1 image (when ImageType is T2 or PD). So enable this config prior to running on non-T1 images.*
- *Nosearch*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Specify that linear registration uses the -nosearch option (FLIRT). --nosearch in fsl_anat.*
- *Nocleanup*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Do not remove intermediate files. --noclean up in fsl_anat.*
  
### Outputs
- Outputs are stored under the **fsl_anat** scan resource. Everytime `fsl-anat` is run, a new sub-folder (date & timestamped to UTC timezone) is created with the outputs pertaining to that run:
  - <img src="./images/output_fs.png" width=25% />
  - [fsl_anat](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/fsl_anat) wiki page describes the main output files that are saved under these folders
  - *log.txt* under the fsl_anat_{month-day-year}_{hour:min:sec} contains a list of all the commands that were run throughout the fsl_anat pipeline
  
### Citation

- M. Jenkinson, C.F. Beckmann, T.E. Behrens, M.W. Woolrich, S.M. Smith. FSL. NeuroImage, 62:782-90, 2012

